from botbuilder.core import ActivityHandler, MessageFactory, TurnContext, ConversationState, UserState, StatePropertyAccessor
from botbuilder.dialogs import Dialog, DialogTurnStatus, DialogSet, ComponentDialog, DialogContext, DialogTurnResult
from botbuilder.schema import ChannelAccount
import json

class DialogBot(ActivityHandler):
    def __init__(
        self,
        conversation_state: ConversationState,
        user_state: UserState,
        dialog: Dialog,
    ):
        if conversation_state is None:
            raise Exception(
                "[DialogBot]: Missing parameter. conversation_state is required"
            )
        if user_state is None:
            raise Exception("[DialogBot]: Missing parameter. user_state is required")
        if dialog is None:
            raise Exception("[DialogBot]: Missing parameter. dialog is required")

        self.conversation_state = conversation_state
        self.user_state = user_state
        self.dialog = dialog
        self.dialogState = self.conversation_state.create_property(
            "DialogState"
        )


    async def on_turn(self, turn_context: TurnContext):
        await super().on_turn(turn_context)

        # Save any state changes that might have occured during the turn.
        await self.conversation_state.save_changes(turn_context, False)
        await self.user_state.save_changes(turn_context, False)

    @staticmethod
    async def run_dialog(
        dialog: Dialog, turn_context: TurnContext, accessor: StatePropertyAccessor
    ):  # pylint: disable=line-too-long
        """Run dialog."""
        dialog_set = DialogSet(accessor)
        dialog_set.add(dialog)

        dialog_context = await dialog_set.create_context(turn_context)
        results = await dialog_context.continue_dialog()

        if results.status == DialogTurnStatus.Empty:
            await dialog_context.begin_dialog(dialog.id, {})


    async def on_members_added_activity(
        self, members_added: [ChannelAccount], turn_context: TurnContext
    ):
        for member in members_added:
            if member.id != turn_context.activity.recipient.id:
                await turn_context.send_activity("Welcome to AutomationEdge chatbot!")

    async def on_message_activity(self, turn_context: TurnContext):
        await self.run_dialog(
            self.dialog,
            turn_context,
            self.conversation_state.create_property("DialogState")
        )

    
