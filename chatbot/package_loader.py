from os import listdir
from os.path import isfile, join
import ast

def load_files(dir_name):
    relative_path = join("chatbot",dir_name)
    all_class_names = []
    all_file_names = []
    for file_name in listdir(relative_path):
        file_path = join(relative_path, file_name)
        if isfile(file_path) and file_name != "__init__.py":
            if file_name.endswith(".py"):
                all_file_names.append(file_name[:-3])
                with open(file_path,"r") as f:
                    p = ast.parse(f.read())
                classes = [c for c in ast.walk(p) if isinstance(c,ast.ClassDef)]
                for class_name in classes:
                    all_class_names.append(class_name.name)

    all_file_names = list(set(all_file_names))
    all_class_names = list(set(all_class_names))
    return (all_file_names, all_class_names)