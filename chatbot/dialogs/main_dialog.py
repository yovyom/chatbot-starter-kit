from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext
)
from botbuilder.dialogs.prompts import TextPrompt, PromptOptions
from botbuilder.core import MessageFactory
from chatbot.dialogs import *
import config

class MainDialog(ComponentDialog):
    def __init__(self, configuration: dict, dialog_id: str = None):
        super(MainDialog, self).__init__(dialog_id or MainDialog.__name__)

        self.configuration = configuration
        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                steps=[
                    self.initial_step
                ]
            )
        )
        self.add_dialog(BookConferenceRoomDialog(self.configuration))
        self.add_dialog(TestDialog(self.configuration))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def initial_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        user_input = step_context.context.activity.text.lower()
        if user_input == "hi" or user_input == "hello":
           await step_context.context.send_activity("Hello, how are you?")
           return await step_context.end_dialog()
        elif user_input == "great" or user_input == "fine" or user_input == "good":
            await step_context.context.send_activity("Great! Glad to hear it.")
            return await step_context.end_dialog()
        elif user_input in config.DefaultBotConfig.KEYWORD_MAPPING.keys():
           return await step_context.begin_dialog(eval(config.DefaultBotConfig.KEYWORD_MAPPING[user_input]+".__name__"), {})
        else:
            await step_context.context.send_activity("Sorry, I couldn't understand what you said.")
            return await step_context.end_dialog()
       