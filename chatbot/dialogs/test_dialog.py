from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    DialogTurnResult,
    WaterfallStepContext
)
from botbuilder.dialogs.prompts import TextPrompt, PromptOptions
from botbuilder.core import MessageFactory
from chatbot.dialogs import CancelAndHelpDialog

class TestDialog(CancelAndHelpDialog):
    def __init__(self, configuration: dict, dialog_id: str = None):
        super(TestDialog, self).__init__(dialog_id or TestDialog.__name__)

        self.configuration = configuration
        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                steps=[
                    self.user_name_step,
                    self.user_location_step,
                    self.final_step
                ]
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__))
        self.initial_dialog_id = WaterfallDialog.__name__

    async def user_name_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("Could you please help me with you name")),
        )
    
    async def user_location_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.options["user_name"] = step_context.result
        return await step_context.prompt(
            TextPrompt.__name__,
            PromptOptions(prompt=MessageFactory.text("Could you please help me with your current location")),
        )

    async def final_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        step_context.options["user_location"] = step_context.result
        await step_context.context.send_activity(f"Hello {step_context.options['user_name']}, I hope the weather in {step_context.options['user_location']} is pleasant today.")
        return await step_context.end_dialog()
    