from botbuilder.dialogs import (
    ComponentDialog,
    WaterfallDialog,
    WaterfallStepContext,
    DialogTurnResult
)
from botbuilder.dialogs.prompts import TextPrompt, PromptOptions
from chatbot.dialogs import CancelAndHelpDialog
from chatbot.helpers import CardHelper, AEHelper
from config import DefaultBotConfig

class BookConferenceRoomDialog(CancelAndHelpDialog):

    def __init__(self, configuration: dict, dialog_id: str = None):
        super(BookConferenceRoomDialog, self).__init__(dialog_id or BookConferenceRoomDialog.__name__)

        self._configuration = configuration

        self.add_dialog(
            WaterfallDialog(
                WaterfallDialog.__name__,
                [
                    self.form_step,
                    self.check_input,
                    self.final_step
                ]
            )
        )
        self.add_dialog(TextPrompt(TextPrompt.__name__))

        self.initial_dialog_id = WaterfallDialog.__name__

    @staticmethod
    def getJsonData(workflow_entities):
        json_data = {
            "orgCode": DefaultBotConfig.AE_ORG_CODE,
            "workflowName": "Book Conference Room",
            "sourceId": None,
            "params": [
                {
                    "name": "date",
                    "value": workflow_entities['date']
                },
                {
                    "name": "startTime",
                    "value": workflow_entities['startTime']
                },
                {
                    "name": "endTime",
                    "value": workflow_entities['endTime']
                },
                {
                    "name": "summary",
                    "value": workflow_entities['summary']
                },
                {
                    "name": "location",
                    "value": workflow_entities['location']
                },
                {
                    "name": "attendees",
                    "value": workflow_entities['attendees']
                }
            ]
        }
        return json_data

    async def form_step(self, step_context: WaterfallStepContext):
        book_conference_room = step_context.options
        response = CardHelper.create_book_conference_room_card(step_context.context.activity, book_conference_room)
        await step_context.context.send_activity(response)
        return ComponentDialog.end_of_turn

    async def check_input(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        if step_context.context.activity.value:
            keys = ["location", "date", "startTime", "endTime", "summary", "attendees"]
            conf_details = step_context.context.activity.value
            flag = False
            for key in keys:
                if key in conf_details.keys() and conf_details[key] != "":

                    step_context.options[key] = conf_details[key]
                else:
                    flag = True
            if flag:
                return await step_context.replace_dialog(self.initial_dialog_id, step_context.options)
            return await step_context.next(step_context.options)
                
    async def final_step(self, step_context: WaterfallStepContext) -> DialogTurnResult:
        await step_context.context.send_activity(str(step_context.options))
        '''#To invoke an AE workflow
        #load the user data as json name-value pairs
        json_data = BookConferenceRoomDialog.getJsonData(step_context.options)
        #pass the data to make the AE execute call
        await AEHelper.execute_workflow(json_data , step_context.context)'''
        return await step_context.end_dialog(step_context.options)