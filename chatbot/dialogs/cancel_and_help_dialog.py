from botbuilder.dialogs import ComponentDialog, DialogContext, DialogTurnResult
from botbuilder.schema import ActivityTypes
from config import DefaultBotConfig

class CancelAndHelpDialog(ComponentDialog):
    async def on_continue_dialog(self, inner_dc: DialogContext) -> DialogTurnResult:
        result = await self.interrupt(inner_dc)
        if result is not None:
            return result

        return await super(CancelAndHelpDialog, self).on_continue_dialog(inner_dc)

    async def interrupt(self, inner_dc: DialogContext) -> DialogTurnResult:
        if inner_dc.context.activity.type == ActivityTypes.message:
            try:
                text = inner_dc.context.activity.text.lower()
                if text in DefaultBotConfig.EXIT_PATTERNS:
                    await inner_dc.context.send_activity("Ok. Talk to you later.")
                    return await inner_dc.cancel_all_dialogs()
            except:
                pass
        return None