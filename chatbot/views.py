import asyncio
import json
from django.http import HttpResponse
from django.apps import apps
from botbuilder.schema import Activity
from botbuilder.core import (
    BotFrameworkAdapter,
    BotFrameworkAdapterSettings,
    TurnContext,
    ConversationState,
    MemoryStorage,
    UserState,
    ShowTypingMiddleware,
)
import config
from chatbot.helpers import BotHelper

def home(request):
    return HttpResponse("Hello, world!")


def messages(request):
    if "application/json" in request.headers["Content-Type"]:
        body = json.loads(request.body.decode("utf-8"))
    else:
        return HttpResponse(status=415)

    activity = Activity().deserialize(body)
    auth_header = (
        request.headers["Authorization"] if "Authorization" in request.headers else ""
    )

    bot_app = apps.get_app_config("chatbot")

    if auth_header == "webChat":
        bot_app.settings = BotFrameworkAdapterSettings("","")
        bot_app.adapter = BotFrameworkAdapter(bot_app.settings)
        bot_app.adapter.use(ShowTypingMiddleware(delay=0.5, period=1.0))
        auth_header = ""
    else:
        bot_app.settings = BotFrameworkAdapterSettings(config.DefaultBotConfig.APP_ID,config.DefaultBotConfig.APP_PASSWORD)
        bot_app.adapter = BotFrameworkAdapter(bot_app.settings)
        bot_app.adapter.use(ShowTypingMiddleware(delay=0.5, period=1.0))

    bot = bot_app.bot
    loop = bot_app.loop
    adapter = bot_app.adapter

    async def aux_func(turn_context):
        await bot.on_turn(turn_context)

    try:
        task = asyncio.ensure_future(
            adapter.process_activity(activity, auth_header, aux_func), loop=loop
        )
        loop.run_until_complete(task)
        return HttpResponse(status=201)
    except Exception as exception:
        raise exception
    return HttpResponse("This is message processing!")

def reply(request):
    """To send replies to chat channels asynchronously.
       First authenticate with bot channels registration service using
       APP_ID and APP_PASSWORD to get ACCESS_TOKEN and then use
       SerivceURL to send the responses
       AEWorkflow should call this to send replies to chat channels asynchronously
    """
    if "application/json" in request.headers["Content-Type"]:
        body = json.loads(request.body.decode("utf-8"))
        logger.debug(f"api/reply recieved body : {body['success']}")
        access_token= BotHelper.authenticate_ms_bot(body)
        if access_token is not None:
            BotHelper.response_ms_bot(body, access_token)
    else:
        return HttpResponse(status=415)
    
    return HttpResponse(status=201)

