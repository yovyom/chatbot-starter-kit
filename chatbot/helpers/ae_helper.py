import requests
import logging
import urllib, http.client
import json
from botbuilder.core import conversation_state, TurnContext
from botbuilder.schema import Activity
from botbuilder.core import (
    BotFrameworkAdapter,
    BotFrameworkAdapterSettings,
    TurnContext,
    ConversationState,
    MemoryStorage,
    UserState
)
from config import DefaultBotConfig
logger = logging.getLogger(__name__)

class AEHelper:
    @staticmethod
    async def authenticate():
        url = f"{DefaultBotConfig.AE_URL}/authenticate"
        data = f"username={DefaultBotConfig.AE_USER}&password={DefaultBotConfig.AE_PASSWORD}"
        response = requests.post(url, data=data,  headers={'content-type': 'application/x-www-form-urlencoded'})
        res_json = response.json()
        if res_json['success']:
            logger.debug(f"Authentication for user: {DefaultBotConfig.AE_USER}, session token: {res_json['sessionToken']}")
            return res_json['sessionToken']
        else:
            logger.debug(f"Authentication for user: {DefaultBotConfig.AE_USER}, failed, error code {response}")
        return None
    
    @staticmethod
    async def execute(session_token, json_data):
        url = f"{DefaultBotConfig.AE_URL}/execute"
        headers = {'X-session-token': session_token ,'content-type': 'application/json'}
        response = requests.post(url, data=json_data,  headers=headers)
        logger.debug(f"into execute class: {response}")
        res_json = response.json()
        if res_json['success']:
            return res_json['automationRequestId']
        return None

    @staticmethod
    def getConversationDetailsPayload(turn_context: TurnContext):
        conversation_details = {
            'chat_channel': turn_context.activity.channel_id, 
            'service_url' : turn_context.activity.service_url+"/v3/conversations/"+turn_context.activity.conversation.id+"/activities",
            'bot_id':turn_context.activity.recipient.id,
            'bot_name': turn_context.activity.recipient.name,
            'user_id': turn_context.activity.from_property.id,
            'user_name': turn_context.activity.from_property.name,
            'conversation_id': turn_context.activity.conversation.id
        }
        additional_info = {}
        additional_info['response_type'] = "chatbot"
        additional_info['conversation_details'] = conversation_details
        return json.dumps(additional_info)

    @staticmethod
    async def execute_workflow(json_data, turn_context: TurnContext):
        session_token = await AEHelper.authenticate()
        json_data["params"].append({"name" : "additionalInfo", "value" : AEHelper.getConversationDetailsPayload(turn_context)})
        response_ae_call = await AEHelper.execute(session_token, json.dumps(json_data))
        if response_ae_call is not None:
            await turn_context.send_activity("Okay got it.Please wait for a moment while I complete your request...")
        else:
            await turn_context.send_activity("Unexpected error occured,Please contact your Administrator")