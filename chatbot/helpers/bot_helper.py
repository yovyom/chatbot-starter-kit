import requests
import logging
import urllib, http.client
import json
from botbuilder.core import conversation_state, TurnContext
from botbuilder.schema import Activity
from botbuilder.core import (
    BotFrameworkAdapter,
    BotFrameworkAdapterSettings,
    TurnContext,
    ConversationState,
    MemoryStorage,
    UserState
)
from chatbot.helpers.card_helper import CardHelper

import config
logger = logging.getLogger(__name__)

class BotHelper:
    @staticmethod
    def authenticate_ms_bot(body):
        conf = config.DefaultBotConfig
        url = f"{conf.MICROSOFT_AUTH_TOKEN_URL}"
        data = f"grant_type=client_credentials&client_id={conf.APP_ID}&client_secret={conf.APP_PASSWORD}&scope=https%3A%2F%2Fapi.botframework.com%2F.default"
        response = requests.post(url, data=data,  headers={'content-type': 'application/x-www-form-urlencoded'})
        res_json = response.json()
        if res_json:
            logger.debug(f"Authentication successfull. Access token is received.")
            return res_json['access_token']
        else:
            logger.debug(f"Authentication fail with MS Bot Reg.{response.statusCode}")
        return None
    
    @staticmethod
    def response_ms_bot(body, access_token:str):
            conversation_body=body['additionalInfo']['conversation_details']
            url= conversation_body['service_url']
            from_details={
                'id': conversation_body['bot_id'],
                'name': conversation_body['bot_name'] 
            }
            conversation_details={
                'id' : conversation_body['conversation_id'],
                'name' : ""
            }
            recipient_details={
                'id': conversation_body['user_id'],
                'name': conversation_body['user_name']
            }
            send_message= BotHelper.format_message(body)
            if body['workflow_name']=="Conference Room Booking":
                json_data = {
                              "type": "message",
                              "from": from_details,
                              "conversation": conversation_details,
                              "recipient": recipient_details,
                              "text": send_message
                        }
                logger.debug(f"workflow_name : {json_data}")
            
            headers = {'Authorization':'Bearer '+access_token,'content-type': 'application/json'}
            response_json_data = json.dumps(json_data)
            response = requests.post(url, data=response_json_data,  headers= headers)
            logger.debug(f"Response request of MS bot :{response}")

    @staticmethod
    def format_message(body):
        execute_success = body['success']
        workflow_name = body['workflow_name']
        if execute_success.lower() == "true":
            if workflow_name == "Conference Room Booking":
                send_message = body["details"]["success_message"]
        else:
            logger.debug(f"execute_success false: {execute_success} ")
            send_message= body['error_details']
            return send_message