from botbuilder.schema import (
    Attachment,
    Activity,
    ActionTypes,
    ReceiptItem,
    Fact,
    ActivityTypes,
    ChannelAccount,
    ConversationAccount,
    AttachmentLayoutTypes
)
from botbuilder.core import CardFactory
from datetime import datetime
import json
import os
class CardHelper:
    @staticmethod
    def load_card(card_file):
        dir_path = os.path.abspath(os.path.dirname(__file__))
        card_path = os.path.join("chatbot", "resources",card_file)
        with open(card_path) as card_file:
            card = json.load(card_file)
        return card

    @staticmethod
    def create_card_attachment(activity: Activity, card_json):
        attachment = Attachment(content_type="application/vnd.microsoft.card.adaptive", content=card_json)
        reply = CardHelper.create_activity_reply(activity)
        reply.attachments = [attachment]
        return reply

    @staticmethod
    def create_activity_reply(activity: Activity, text: str = None, locale: str = None):
        """Helper to create reply object."""
        return Activity(
            type=ActivityTypes.message,
            timestamp=datetime.utcnow(),
            from_property=ChannelAccount(
                id=getattr(activity.recipient, "id", None),
                name=getattr(activity.recipient, "name", None),
            ),
            recipient=ChannelAccount(
                id=activity.from_property.id, name=activity.from_property.name
            ),
            reply_to_id=activity.id,
            service_url=activity.service_url,
            channel_id=activity.channel_id,
            conversation=ConversationAccount(
                is_group=activity.conversation.is_group,
                id=activity.conversation.id,
                name=activity.conversation.name,
            ),
            text=text or "",
            locale=locale or "",
            attachments=[],
            entities=[],
        )

    @staticmethod
    def create_book_conference_room_card(activity, conf_details: dict):
        card = CardHelper.load_card("book_conference_room.json")
        conf_rname = ["Singapore", "New York", "Zurich", "Rajdhani"]
        conf_rname_choices = []
        for name in conf_rname:
            conf_rname_choices.append({"title":name, "value":name.lower()})
        card["body"][2]["choices"] = conf_rname_choices
        if "attendees" in conf_details.keys():
            card["body"][9]["value"] = conf_details["attendees"]
        if "location" in conf_details.keys():
            card["body"][2]["value"] = conf_details["location"]
        if "date" in conf_details.keys():
            card["body"][4]["value"] = conf_details["date"]
        if "startTime" in conf_details.keys():
            card["body"][5]["columns"][0]["items"][1]["value"] = conf_details["startTime"]
        if "endTime" in conf_details.keys():
            card["body"][5]["columns"][1]["items"][1]["value"] = conf_details["endTime"]
        if "summary" in conf_details.keys():
            card["body"][7]["value"] = conf_details["summary"]
        reply = CardHelper.create_card_attachment(activity, card)
        return reply