import sys
import json
import asyncio
from django.apps import AppConfig
from botbuilder.schema import Activity
from botbuilder.core import (
    BotFrameworkAdapter,
    BotFrameworkAdapterSettings,
    TurnContext,
    ConversationState,
    MemoryStorage,
    UserState,
    ShowTypingMiddleware
)
from chatbot.bot import DialogBot
import config
from chatbot.dialogs import MainDialog

class ChatbotConfig(AppConfig):
    name = "chatbot"
    
    app_config = config.DefaultBotConfig
    settings = BotFrameworkAdapterSettings(app_config.APP_ID, app_config.APP_PASSWORD)
    adapter = BotFrameworkAdapter(settings)
    adapter.use(ShowTypingMiddleware(delay=0.5, period=1.0))
    loop = asyncio.get_event_loop()
    memory_storage = MemoryStorage()

    user_state = UserState(memory_storage)
    conversation_state = ConversationState(memory_storage)
    dialog = MainDialog(app_config)
    bot = DialogBot(conversation_state, user_state, dialog)

    async def on_error(self, context: TurnContext, error: Exception):
        # TODO
        # (1) Log error using application logging mechanism
        # (2) Log this to Azure Application Insights
        print(f"\n [on_turn_error]: { error }", file=sys.stderr)
        await context.send_activity("Oops, something went wrong!")
        await self.conversation_state.delete(context)
    
    def ready(self):
        self.adapter.on_turn_error = self.on_error
