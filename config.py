class DefaultBotConfig(object):
	PORT = 8000
	APP_ID = ""
	APP_PASSWORD = ""

	AE_URL = ""
	AE_USER = ""
	AE_PASSWORD = ""
	AE_ORG_CODE = ""
	MICROSOFT_AUTH_TOKEN_URL="https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token"

	KEYWORD_MAPPING = {
		"book conference room" : "BookConferenceRoomDialog",
		"test": "TestDialog"
	}

	EXIT_PATTERNS = ["quit", "/q", "cancel"]